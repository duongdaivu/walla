$(document).ready(function(){
    var tableArray = [];
    var tableCommon = $("table.dataTableCommon").DataTable({
        responsive:false
    });
    tableArray.push(tableCommon);
    // Config for table display orders lists
    if ($('#dataTableOrdersAll').length) {
        var table = $('#dataTableOrdersAll').DataTable({
            responsive:false,
        });
        table.buttons().container().appendTo($('#dataTableOrdersAll_length', table.table().container()));
        tableArray.push(table);
    }

    // Apply search
    tableArray.forEach(function (item) {
        item.columns().every(function (){
            var that = this;
            var typeList = ['input', 'select'];
            configSearchIndividual(that, typeList);
        });
    });
});

// Function handle search for individual column
function configSearchIndividual(obj, typeList)
{
    typeList.forEach(function (type){
        $(type, obj.footer()).on('keyup change', function () {
            if (obj.search() !== this.value) {
                obj
                    .search(this.value)
                    .draw();
            }
        });
    });
}