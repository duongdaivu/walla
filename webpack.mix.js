const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const vendors = 'node_modules/';
const resourcesAssets = 'resources/assets/';
const srcCss = resourcesAssets + 'css/';
const srcJs = resourcesAssets + 'js/';
const srcSass = resourcesAssets + 'sass/';

//destination path configuration
const dest = 'public/';
const destFonts = dest + 'fonts/';
const destWebFonts = dest + 'webfonts/';
const destCss = dest + 'css/';
const destJs = dest + 'js/';
const destImages = dest + 'images/';
const destVendors= dest + 'vendors/';

const paths = {
    'jquery' : vendors + 'jquery/dist/',
    'popperjs': vendors + 'popper.js/dist/umd/',
    'bootstrap': vendors + 'bootstrap/dist/',
    'formBuilder' : vendors + 'js-beautify/js/lib/',
    'wysihtml5': vendors + 'bootstrap3-wysihtml5-bower/dist/',
    'bootstraprating': vendors  + 'bootstrap-rating/',
    'daterangepicker' : vendors + 'bootstrap-daterangepicker/',
    'markdown' : vendors + 'bootstrap-markdown/',
    'maxlength' : vendors + 'bootstrap-maxlength/src/',
    'multiselect' :vendors + 'bootstrap-multiselect/dist/',
    'progressbar': vendors +'bootstrap-progressbar/',
    'rating' :vendors + 'bootstrap-rating/',
    'switch': vendors + 'bootstrap-switch/dist/',
    'tagsinput' : vendors + 'bootstrap-tagsinput/',
    'typeaheadjs' : vendors + 'typeahead.js/dist/',
    'timepicker' : vendors + 'bootstrap-timepicker/',
    'touchspin': vendors +'bootstrap-touchspin/dist/',
    'ckeditor': vendors +'ckeditor/',
    'fontawesome':vendors + '@fortawesome/fontawesome-free/',
    'dataTables': vendors + 'datatables/media',
    'devicon': vendors +'devicon/',
    'html5sortable' : vendors + 'html5sortable/dist/',
    'ionicons' : vendors + 'ionicons/dist/',
    'inputmask': vendors +'inputmask/dist/',
    'select2': vendors + 'select2/dist/',
    'select2BootstrapTheme': vendors + 'select2-bootstrap-theme/dist/',
    'fullcalendar': vendors + 'fullcalendar/dist/',
    'jqueryui' : vendors + 'jquery-ui/',
    'colorpicker': vendors +'bootstrap-colorpicker/dist/',
    'moment' : vendors + 'moment/',
    'timezone' : vendors + 'moment-timezone/',
    'bootstrapSlider': vendors +'bootstrap-slider/dist/',
    'sortablejs': vendors +'sortablejs/',
    'datatables' : vendors + 'datatables.net/',
    'datatablesbs4' : vendors + 'datatables.net-bs4/',
    'datatablesbutton' : vendors + 'datatables.net-buttons/',
    'datatablesbuttonsbs4' : vendors + 'datatables.net-buttons-bs4/',
    'datatablescolreorder' : vendors + 'datatables.net-colreorder/',
    'datatablescolreorderbs4' : vendors + 'datatables.net-colreorder-bs4/',
    'datatablesresponsive' : vendors + 'datatables.net-responsive/',
    'datatablesrowreorder' : vendors + 'datatables.net-rowreorder/',
    'datatablesrowreorderbs4' : vendors + 'datatables.net-rowreorder-bs4/',
    'datatablesscroll' : vendors + 'datatables.net-scroller/',
    'datatablesscrollbs4' : vendors + 'datatables.net-scroller-bs4/',
    'datepicker' : vendors + 'bootstrap-datepicker/dist/',
    'wow' : vendors + 'wowjs/dist/',
    'raphael' : vendors + 'raphael/',
    'pdfmake' : vendors + 'pdfmake/build/',
    'awesomebootstrapcheckbox' :vendors + 'awesome-bootstrap-checkbox/',
    'bootstrapStarRating' : vendors + 'bootstrap-star-rating/',
    'pickadate' : vendors + 'pickadate/lib/',
    'hover': vendors + 'hover.css/css/',
    'bootstrapdatetimepicker': vendors + 'bootstrap-datetime-picker/',
    'jTable': vendors +'jtable/'
};

mix.options({
    processCssUrls: false
});

// ionicons
mix.copy(paths.ionicons + 'css/ionicons.min.css', destVendors + 'ionicons/css');
mix.copy(paths.ionicons + 'fonts', destVendors + 'ionicons/fonts');

// fonts straight to public
mix.copy(paths.fontawesome + 'webfonts', destWebFonts);

// daterange picker
mix.copy(paths.daterangepicker + 'daterangepicker.js', destVendors + 'daterangepicker/js');
mix.copy(paths.daterangepicker + 'daterangepicker.css',  destVendors + 'daterangepicker/css');

// datetime picker
mix.copy(paths.bootstrapdatetimepicker + '/css/bootstrap-datetimepicker.min.css',  destVendors + 'bootstrap-datetime-picker/css');
mix.copy(paths.bootstrapdatetimepicker + '/js/bootstrap-datetimepicker.min.js', destVendors + 'bootstrap-datetime-picker/js');

//pickadate
mix.copy(paths.pickadate + '/themes/default.css',  destVendors + 'pickadate/css');
mix.copy(paths.pickadate + '/themes/default.date.css',  destVendors + 'pickadate/css');
mix.copy(paths.pickadate + '/themes/default.time.css',  destVendors + 'pickadate/css');
mix.copy(paths.pickadate + 'picker.js', destVendors + 'pickadate/js');
mix.copy(paths.pickadate + 'picker.date.js', destVendors + 'pickadate/js');
mix.copy(paths.pickadate + 'picker.time.js', destVendors + 'pickadate/js');

// awesomebootstrapcheckbox
mix.copy(paths.awesomebootstrapcheckbox + 'awesome-bootstrap-checkbox.css', destVendors + 'awesomebootstrapcheckbox/css');
mix.copy(paths.awesomebootstrapcheckbox + 'demo/build.css', destVendors + 'awesomebootstrapcheckbox/css');

//bootsdtrap starrating
mix.copy(paths.bootstrapStarRating + 'css/star-rating.min.css', destVendors + 'bootstrapRating/css' );
mix.copy(paths.bootstrapStarRating + 'js/star-rating.min.js', destVendors + 'bootstrapRating/js'  );
mix.copy(paths.bootstrapStarRating + 'img',  destVendors + 'bootstrapStarRating/img');
mix.copy(paths.bootstrapStarRating + 'themes/krajee-fa',  destVendors + 'bootstrapStarRating/themes');
mix.copy( srcJs + 'pages/custom_rating.js', destJs + 'pages');
mix.copy( srcCss + 'pages/custom_rating.css', destCss + 'pages');

// awesomebootstrapcheckbox
mix.copy(paths.awesomebootstrapcheckbox + 'awesome-bootstrap-checkbox.css', destVendors + 'awesomebootstrapcheckbox/css');
mix.copy(paths.awesomebootstrapcheckbox + 'demo/build.css', destVendors + 'awesomebootstrapcheckbox/css');

// bootstrap3-wysihtml5-bower
mix.copy(paths.wysihtml5 + 'bootstrap3-wysihtml5.min.css',  destVendors + 'bootstrap3-wysihtml5-bower/css');
mix.copy(paths.wysihtml5 + 'bootstrap3-wysihtml5.all.min.js',  destVendors + 'bootstrap3-wysihtml5-bower/js');
mix.copy(paths.wysihtml5 + 'bootstrap3-wysihtml5.min.js',  destVendors + 'bootstrap3-wysihtml5-bower/js');

// ckeitor
mix.copy(paths.ckeditor + 'ckeditor.js',  destVendors + 'ckeditor/js');
mix.copy(paths.ckeditor + 'adapters/jquery.js',  destVendors + 'ckeditor/js');
mix.copy(paths.ckeditor + 'config.js',  destVendors + 'ckeditor/js');
mix.copy(paths.ckeditor + 'skins/',  destVendors + 'ckeditor/js/skins',false);
mix.copy(paths.ckeditor + 'plugins/',  destVendors + 'ckeditor/js/plugins');
mix.copy(paths.ckeditor + 'lang',  destVendors + 'ckeditor/js/lang');
mix.copy(paths.ckeditor + 'styles.js',  destVendors + 'ckeditor/js');
mix.copy(paths.ckeditor + 'contents.css',  destVendors + 'ckeditor/js');

//select2
mix.copy(paths.select2 + 'css/select2.min.css',  destVendors + 'select2/css');
mix.copy(paths.select2 + 'js/select2.js',  destVendors + 'select2/js');
mix.copy(paths.select2 + 'js/select2.full.js',  destVendors + 'select2/js');
mix.copy(paths.select2BootstrapTheme + 'select2-bootstrap.css',  destVendors + 'select2/css');

//hover
mix.copy(paths.hover + 'hover-min.css', destVendors + 'hover/css');

//moment
mix.copy(paths.moment + 'min/moment.min.js',  destVendors + 'moment/js');

//bootstrap color picker
mix.copy(paths.colorpicker + 'css/bootstrap-colorpicker.min.css',  destVendors + 'colorpicker/css');
mix.copy(paths.colorpicker + 'js/bootstrap-colorpicker.min.js',  destVendors + 'colorpicker/js');
mix.copy(paths.colorpicker + 'img/bootstrap-colorpicker',  destVendors + 'colorpicker/img/bootstrap-colorpicker');

// bootstrap tagsinput
mix.copy(paths.tagsinput + 'dist/bootstrap-tagsinput.css',  destVendors + 'bootstrap-tagsinput/css');
mix.copy(paths.tagsinput + 'examples/assets/app.css',  destVendors + 'bootstrap-tagsinput/css');
mix.copy(paths.tagsinput + 'examples/assets/app_bs3.js',  destVendors + 'bootstrap-tagsinput/js');
mix.copy(paths.tagsinput + 'dist/bootstrap-tagsinput.js',  destVendors + 'bootstrap-tagsinput/js');

//sortable list
mix.copy( srcCss + 'pages/sortable_list.css', destCss + 'pages');
mix.copy(paths.sortablejs + 'Sortable.js',  destVendors + 'sortablejs/js');
mix.copy( srcJs + 'pages/sortable_list.js', destJs + 'pages');

// bootstrap progressbar
mix.copy(paths.progressbar + 'css/bootstrap-progressbar-3.3.4.css',  destVendors + 'bootstrap-progressbar/css');
mix.copy(paths.progressbar + 'bootstrap-progressbar.js',  destVendors + 'bootstrap-progressbar/js');

// bootstrap touchspin
mix.copy(paths.touchspin + 'jquery.bootstrap-touchspin.css',  destVendors + 'bootstrap-touchspin/css');
mix.copy(paths.touchspin + 'jquery.bootstrap-touchspin.js',  destVendors + 'bootstrap-touchspin/js');

// // bootstrap multiselect
mix.copy(paths.multiselect + 'css/bootstrap-multiselect.css',  destVendors + 'bootstrap-multiselect/css');
mix.copy(paths.multiselect + 'js/bootstrap-multiselect.js',  destVendors + 'bootstrap-multiselect/js');

// bootstrap switch
mix.copy(paths.switch + 'css/bootstrap3/bootstrap-switch.css',  destVendors + 'bootstrap-switch/css');
mix.copy(paths.switch + 'js/bootstrap-switch.js',  destVendors + 'bootstrap-switch/js');

// font-awesome
//mix.copy(paths.fontawesome + 'css/all.css', 'public/css/font');
mix.copy( srcJs + 'pages/icons.js', destJs + 'pages');

// bootstrap-timepicker
mix.copy(paths.timepicker + 'css/timepicker.less',  destVendors + 'bootstrap-timepicker/css');
mix.copy(paths.timepicker + 'js/bootstrap-timepicker.js',  destVendors + 'bootstrap-timepicker/js');

// seiyria-bootstrap-slider
mix.copy(paths.bootstrapSlider + 'css/bootstrap-slider.min.css',  destVendors + 'bootstrap-slider/css');
mix.copy(paths.bootstrapSlider + 'bootstrap-slider.js',  destVendors + 'bootstrap-slider/js');

// bootstrap-datepicker
mix.copy(paths.datepicker + 'js/bootstrap-datepicker.js', destVendors + 'bootstrap-datepicker/js');
mix.copy(paths.datepicker + 'css/bootstrap-datepicker.css', destVendors + 'bootstrap-datepicker/css');

//bootstrap-maxlength
mix.copy(paths.maxlength + 'bootstrap-maxlength.js',  destVendors + 'bootstrap-maxlength/js');

//datatables
mix.copy(paths.datatables + 'js/jquery.dataTables.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbs4 + 'js/dataTables.bootstrap4.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbs4 + 'css/dataTables.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesbutton + 'js/buttons.print.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbutton + 'js/dataTables.buttons.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbuttonsbs4 + 'css/buttons.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesbuttonsbs4 + 'js/buttons.bootstrap4.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablescolreorder + 'js/dataTables.colReorder.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablescolreorderbs4 + 'css/colReorder.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesresponsive + 'js/dataTables.responsive.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesrowreorder + 'js/dataTables.rowReorder.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbutton + 'js/buttons.html5.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbutton + 'js/buttons.colVis.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbutton + 'js/buttons.print.min.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesrowreorderbs4 + 'css/rowReorder.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesscroll + 'js/dataTables.scroller.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesscrollbs4 + 'css/scroller.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.pdfmake + 'pdfmake.js',  destVendors + 'datatables/js');
mix.copy(paths.pdfmake + 'vfs_fonts.js',  destVendors + 'datatables/js');

//datatables page
mix.copy( srcJs + 'pages/table-advanced.js', destJs + 'pages');
mix.copy( srcCss + 'pages/tables.css', destCss + 'pages');
mix.copy( srcJs + 'pages/table-advanced2.js', destJs + 'pages');
mix.copy( srcJs + 'pages/table-editable.js', destJs + 'pages');
mix.copy( srcJs + 'pages/data.txt', destJs + 'pages');
mix.copy( srcJs + 'pages/table-responsive.js', destJs + 'pages');

// fullcalendar
mix.copy(paths.fullcalendar + 'fullcalendar.css', destVendors + 'fullcalendar/css');
mix.copy(paths.fullcalendar + 'fullcalendar.print.css', destVendors + 'fullcalendar/css');
mix.copy(paths.fullcalendar + 'fullcalendar.min.js', destVendors + 'fullcalendar/js');

// wow
mix.copy(paths.wow + 'wow.min.js',  destVendors + 'wow/js');

// metis menu
mix.copy( srcJs + 'metisMenu.js', destJs);

// buttons css
mix.sass(srcSass + 'buttons/buttons.scss', destCss + 'buttons.css');
mix.sass(resourcesAssets + 'sass/app.scss', destCss + 'bootstrap.css');
mix.sass(srcSass + 'buttons/awesome-bootstrap-checkbox.scss', destCss + 'plugins/awesome-bootstrap-checkbox.css');

// all global css files into app.css
mix.combine(
    [
        destCss + 'bootstrap.css',
        paths.fontawesome + 'css/all.min.css',
        srcCss + 'metisMenu.css',
        srcCss + 'custom.css',
        srcCss + 'pages/custom.css'
    ], destCss + 'app.css');

// all global js files into app.js
mix.combine(
    [
        paths.jquery + 'jquery.min.js',
        srcJs + 'pages/jquery-ui.min.js',
        paths.popperjs + 'popper.min.js',
        paths.bootstrap + 'js/bootstrap.min.js',
        vendors + 'raphael/raphael.min.js',
        srcJs + 'metisMenu.js',
        srcJs + 'josh.js',
        srcJs + 'jquery-slimscroll.js',
        vendors + 'holderjs/holder.min.js'
    ], destJs + 'app.js');